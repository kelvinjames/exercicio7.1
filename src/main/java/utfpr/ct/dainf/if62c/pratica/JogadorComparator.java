/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.Comparator;

/**
 *
 * @author kelvin
 */
public class JogadorComparator implements Comparator<Jogador> {
    Boolean ordena;
    Boolean numeroAscendente;
    Boolean nomeAscendente;
    public JogadorComparator(){
        this.ordena = true;
        this.nomeAscendente = true;
        this.numeroAscendente = true;
    }
    public JogadorComparator(boolean ordena,boolean numeroAscendente,boolean nomeAscendente){
        this.ordena = ordena;
        this.numeroAscendente = numeroAscendente;
        this.nomeAscendente = nomeAscendente;
    }
    @Override
    public int compare(Jogador jogador1,Jogador jogador2){
        int retorno;
        if(this.ordena == true){
            if(jogador1.numero == jogador2.numero){
                retorno = jogador1.nome.compareTo(jogador2.nome);
                if(!nomeAscendente){
                    retorno *= -1;
                }
            }
            else{
                retorno = jogador1.compareTo(jogador2);
                if(!numeroAscendente){
                    retorno *= -1;
                }
            }
        }
        else{
            if(jogador1.nome.equals(jogador2.nome)){
                retorno = jogador1.compareTo(jogador2);
                if(!numeroAscendente){
                    retorno = retorno * -1;
                }
            }
            else{
                retorno = jogador1.nome.compareTo(jogador2.nome);
                if(!nomeAscendente){
                    retorno = retorno * -1;
                }
            }
        }
        return retorno;
    }
}
